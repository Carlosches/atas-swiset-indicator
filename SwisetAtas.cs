﻿using ATAS.Indicators;
using ATAS.DataFeedsCore;
using System.Text;
using System;
using System.Threading;
using System.IO;
using System.Net;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace atas_swiset_indicator
{
    public class SwisetAtas : Indicator
    {

        #region Constants
        private const string SWISET_LOGS = "ATAS-Swiset Logs.txt";
        private const string version = "3.0";
        private const string platformId = "4";
        #endregion

        #region Attributes
        private List<string> RegisteredTrades;
        private RequestController REQUESTS;
        #endregion

        #region Parameters

        [Display(GroupName = "Common", Name = "Automation Token", Order = 10)]
        public string swToken { get; set; }
        #endregion

        

        public SwisetAtas()
        {
            EnableCustomDrawing = true;
            SubscribeToDrawingEvents(DrawingLayouts.Final);
        }
        public static void Main(string[] args)
        {
            
        }

        protected override void OnInitialize()
        {

            REQUESTS = new RequestController();
            RegisteredTrades = new List<string>();
            updateLastActivity();
            RetrieveRegisterTrades();
            Task.Run(() => ReviewHistoricTrades());

        }

        protected override void OnNewMyTrade(MyTrade myTrade) {
            try
            {
                decimal price = myTrade.Price;
                DateTime time = myTrade.Time;
                string securityCode = myTrade.Security.Code;
                string accountId = myTrade.AccountID;
                var historyTrades = TradingStatisticsProvider.Realtime.HistoryMyTrades;
              
                var historyTradeFound = historyTrades.FirstOrDefault(trade => price == trade.ClosePrice && time == trade.CloseTime && securityCode == myTrade.Security.Code && accountId == myTrade.AccountID);

                if (historyTradeFound != null) { 
                    if (RegisterTrade(historyTradeFound))
                        PersistTrade("" + historyTradeFound.Id);
                    
                }

            }
            catch (Exception) { }
        }

            private  void RetrieveRegisterTrades()
        {
            try
            {
                var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), SWISET_LOGS);
                using (var reader = new StreamReader(path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    { 
                       RegisteredTrades.Add(line.Trim());
                    }
                        
                }
            }
            catch (Exception) { }
        }

        private void PersistTrade(string orderId)
        {
            try
            {
                var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), SWISET_LOGS);
                using (var writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(orderId);
                    writer.Flush();
                }
                //this.LogWarn("PersistTrade: " + orderId);
                RegisteredTrades.Add(orderId);
            }
            catch (Exception) { }
        }

        public void ReviewHistoricTrades()
        {
            while (true)
            { 
                ProcessHistoricTrades();
                Thread.Sleep(120000); // every 3 minutes
            }

        }
        private void ProcessHistoricTrades()
        {
            try
            {
                var historyTrades = TradingStatisticsProvider.Realtime.HistoryMyTrades;
               
                foreach (var historyTrade in historyTrades)
                {
                    
                    bool ver = VerifyNotRegistered("" + historyTrade.Id);
                    if (ver)
                    {
                        if (RegisterTrade(historyTrade))
                            PersistTrade("" + historyTrade.Id);
                    }
                   
                }

            }catch(Exception){}
        }

        public bool VerifyNotRegistered(string orderId)
        {
            var found = RegisteredTrades.FirstOrDefault(positionId => positionId.Equals(orderId));
            return found == null;
        }

        
        public bool RegisterTrade(HistoryMyTrade trade)
        {
            try
            {
                var buySide = (trade.PnL > 0 & trade.ClosePrice > trade.OpenPrice) | (trade.PnL < 0 & trade.OpenPrice > trade.ClosePrice);
                var priceDifference = (buySide) ? trade.ClosePrice - trade.OpenPrice : trade.OpenPrice - trade.ClosePrice;
                var stopLossPrice = (trade.PnL < 0) ? trade.ClosePrice : trade.OpenPrice - Math.Abs(priceDifference);
                double commision = 0;
                if (trade.Commission != null)
                    commision = (double)trade.Commission;
                var grossProfit = Math.Abs((double) trade.PnL) + Math.Abs(commision);
                var pipValue = (priceDifference == 0) ? 0 : grossProfit / Math.Abs((double) priceDifference);
                TradeFromScript tradeToExport =
                    new TradeFromScript(
                        swToken, // Automation Token
                        trade.Security.Exchange, // BrokerW
                        trade.AccountID, // Account Name
                       ((double)trade.Portfolio.Balance), // 
                        "ATAS", // Platform
                        trade.Id + "", // Trade Sequence
                        trade.Security.Code, // Symbol Name
                        buySide? "BUY" : "SELL",
                        trade.OpenTime.ToLocalTime().ToString("yyyy-MM-ddTHH\\:mm\\:ssZ"), // Open Time
                        trade.CloseTime.ToLocalTime().ToString("yyyy-MM-ddTHH\\:mm\\:ssZ"), // Close Time
                        (double) trade.OpenPrice, // Entry Price
                        (double)trade.ClosePrice, // Close Price
                        (double)stopLossPrice, // Stop Loss Price
                        (double) trade.PnL,//trade.ProfitCurrency, // Net Profit
                        pipValue, // Pip Value
                        ((double)commision), // Commissions
                        (int) trade.OpenVolume,//trade.Quantity, 
                        !SameDay(trade.OpenTime, trade.CloseTime),//!SameDay(trade.Entry.Time, trade.Exit.Time), // IsOvernight
                        trade.Portfolio.IsRealAccount
                        );

                // Create a stream to serialize the object to.
                var ms = new MemoryStream();
                //this.LogWarn("Account balance: " + tradeToExport.accountBalance + "\n" + "IsRealAccount" + tradeToExport.realAccount);
                // Serializer the User object to the stream.
                var ser = new DataContractJsonSerializer(typeof(TradeFromScript));
                ser.WriteObject(ms, tradeToExport);
                byte[] json = ms.ToArray();
                ms.Close();
                var jsonData = Encoding.UTF8.GetString(json, 0, json.Length);
                REQUESTS.postRequest(jsonData,false);
               
                return true;
            }
            catch (Exception )
            { 
                return false;
            }
        }
        public bool SameDay(DateTime dateA, DateTime dateB)
        {
            return (dateA.Year == dateB.Year) && (dateA.Month == dateB.Month) && (dateA.Day == dateB.Day);
        }
        public void updateLastActivity()
        {
            string data = "{" +
                  "\"automationToken\": " + "\"" + swToken + "\"," +
                   "\"version\": " + "\"" +  version+ "\"," +
                  "\"platformID\": " + "\"" + platformId + "\"" +
                "}";
            REQUESTS.postRequest(data, true);
        }
        /*
         * Do not remove this method
         */
        protected override void OnCalculate(int bar, decimal value)
        {

        }
    }
        [DataContract()]
        public class TradeFromScript
        {
            [DataMember()]
            public string swtToken { get; private set; }
            [DataMember()] 
            public string brokerName { get; private set; }
            [DataMember()] 
            public string brokerAccountIdentifier { get; private set; }
            [DataMember()] 
            public double accountBalance { get; private set; }
            [DataMember()] 
            public string platform { get; private set; }

            // Trade Related Data
            [DataMember()] 
            public string tradeId { get; private set; }
            [DataMember()]
            public string assetTicker { get; private set; }
            [DataMember()] 
            public string side { get; private set; }
            [DataMember()]
            public string openTime { get; private set; }
            [DataMember()]
            public string closeTime { get; private set; }
            [DataMember()]
            public double entryPrice { get; private set; }
            [DataMember()]
            public double closePrice { get; private set; }
            [DataMember()]
            public double stopLossPrice { get; private set; }
            [DataMember()]
            public double netProfit { get; private set; }
            [DataMember()]
            public double pipValue { get; private set; }
            [DataMember()]
            public double commission { get; private set; }
            [DataMember()]
            public int contracts { get; private set; }
            [DataMember()]
            public bool overnight { get; private set; }
            [DataMember()]
            public bool realAccount { get; private set; }

            public TradeFromScript(string swtToken, string brokerName, string brokerAccountIdentifier, double accountBalance, string platform, string tradeId, string assetTicker, string side, string openTime, string closeTime, double entryPrice, double closePrice,
            double stopLossPrice, double netProfit, double pipValue, double commission, int contracts, bool overnight, bool realAccount)
            {
                this.swtToken = swtToken;
                this.brokerName = brokerName;
                this.brokerAccountIdentifier = brokerAccountIdentifier;
                this.accountBalance = accountBalance;
                this.platform = platform;
                this.tradeId = tradeId;
                this.assetTicker = assetTicker;
                this.side = side;
                this.openTime = openTime;
                this.closeTime = closeTime;
                this.entryPrice = entryPrice;
                this.closePrice = closePrice;
                this.stopLossPrice = stopLossPrice;
                this.netProfit = netProfit;
                this.pipValue = pipValue;
                this.commission = commission;
                this.contracts = contracts;
                this.overnight = overnight;
                 this.realAccount = realAccount;
            }
        }
        public class RequestController
        {

        //private const string SWISET_API = "http://localhost:8080/api";
        private const string SWISET_API = "https://api.swiset.com/api";
        private const string UPDATE_ACTIVITY_URL = "/linked-platform/update-activity";
        private const string ROUTE_ADD_TRADE = "/ext/trades/add-by-script";
        private string currentURL;
        public string postRequest(string data, bool updLastActivity, string method = "POST")
            {

                byte[] dataBytes = Encoding.UTF8.GetBytes(data);
                currentURL = ROUTE_ADD_TRADE;
                if (updLastActivity)
                {
                    currentURL = UPDATE_ACTIVITY_URL;
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", SWISET_API, currentURL));
                request.Timeout = Timeout.Infinite;
                request.KeepAlive = true;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = "application/json";
                request.Method = "POST";

                using (Stream requestBody = request.GetRequestStream())
                    requestBody.Write(dataBytes, 0, dataBytes.Length);

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }

